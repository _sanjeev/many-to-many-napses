const express = require("express");
const { sequelize, User, Profile } = require("./models");

const app = express();
app.use(express.json());

app.post("/users", async (req, res) => {
  const { username, points } = req.body;
  try {
    const user = await User.create({ username, points });
    return res.json(user);
  } catch (error) {
    console.log(error);
    return res.json(error);
  }
});

app.get("/profiles", async (req, res) => {
  try {
    const profile = await Profile.findAll();
    return res.json(profile);
  } catch (error) {
    console.log(error);
    return res.json(error);
  }
});

app.post("/users/:id/profiles", async (req, res) => {
  const id = req.params.id;
  const { name } = req.body;
  try {
    const user = await User.findOne({ where: { id } });
    const profile = await Profile.create({ name });
    await user.addProfile(profile, { through: { selfGranted: false } });
    return res.json(profile);
  } catch (error) {
    console.log(error);
    return res.json(error);
  }
});

app.post("/profiles/:id/users", async (req, res) => {
  const id = req.params.id;
  const { username, points } = req.body;
  try {
    const profile = await Profile.findOne({ where: { id } });
    const user = await User.create({ username, points });
    await profile.addUser(User, { through: { selfGranted: false } });
    return res.json(user);
  } catch (error) {
    console.log(error);
    return res.json(error);
  }
});

app.get("/profiles/:id/users", async (req, res) => {
  const id = req.params.id;
  const { username, points } = req.body;
  try {
    const profile = await Profile.findOne({ where: { id }, include: [User] });
    // const user = await User.create({ username, points });
    // await profile.addUser(User, { through: { selfGranted: false } });
    return res.json(profile);
  } catch (error) {
    console.log(error);
    return res.json(error);
  }
});

app.get("/users/:id/profiles", async (req, res) => {
  const id = req.params.id;
  try {
    const user = await User.findOne({ where: { id }, include: [Profile] });
    // const profile = await Profile.create({name});
    // await user.addProfile(profile, { through: { selfGranted: false } });
    return res.json(user);
  } catch (error) {
    console.log(error);
    return res.json(error);
  }
});

app.get("/profiles/:id/users", async (req, res) => {
  const id = req.params.id;
  try {
    const profile = await Profile.findOne({ where: { id }, include: [User] });
    // const profile = await Profile.create({name});
    // await user.addProfile(profile, { through: { selfGranted: false } });
    return res.json(profile);
  } catch (error) {
    console.log(error);
    return res.json(error);
  }
});

// app.post ('/users/:id/profiles', async (req, res) => {
//   const id = req.params.id;
//   const {name} = req.body;
//   try {
//     const user = await User.findOne({
//       where :{ id }
//     })
//     const profiles = await Profile.create({name, userId: id});
//     return res.json(profiles);
//   } catch (error) {
//     console.log(error);
//     return res.json(error);
//   }
// })

// app.get ('/users/:id/profiles', async (req, res) => {
//   const id = req.params.id;
//   try {
//     const user = await User.findOne({
//       where :{ id },
//       include: [Profile]
//     })
//     // const profiles = await Profile.create({name, userId: id});
//     return res.json(user);
//   } catch (error) {
//     console.log(error);
//     return res.json(error);
//   }
// })

// app.get ('/users/:id/profiles', async (req, res) => {
//   const id = req.params.id;
//   // const {name} = req.body;
//   try {
//     const user = await User.findOne({
//       where :{ id },
//       include: [Profile]
//     })
//     // const profies = await Profile.create({name, profileId: user.id});
//     return res.json(user);
//   } catch (error) {
//     console.log(error);
//     return res.json(error);
//   }
// })

sequelize.sync({ alter: true }).then(() => {
  app.listen({ port: 4000 }, async () => {
    console.log("Server hosted on port 4000");
    await sequelize.authenticate();

    console.log("Databse Connected");
  });
});
