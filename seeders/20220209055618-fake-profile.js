"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "profiles",
      [
        {
          name: "Software Developer",
          updatedAt: "2022-02-09T05:53:59.884Z",
          createdAt: "2022-02-09T05:53:59.884Z",
        },
        {
          name: "Software Engineer",
          updatedAt: "2022-02-09T05:53:59.884Z",
          createdAt: "2022-02-09T05:53:59.884Z",
        },
        {
          name: "Software Tester",
          updatedAt: "2022-02-09T05:53:59.884Z",
          createdAt: "2022-02-09T05:53:59.884Z",
        },
        {
          name: "Principal Developer",
          updatedAt: "2022-02-09T05:53:59.884Z",
          createdAt: "2022-02-09T05:53:59.884Z",
        },
        {
          name: "Developer",
          updatedAt: "2022-02-09T05:53:59.884Z",
          createdAt: "2022-02-09T05:53:59.884Z",
        },
        {
          name: "SDE1",
          updatedAt: "2022-02-09T05:53:59.884Z",
          createdAt: "2022-02-09T05:53:59.884Z",
        },
        {
          name: "SDE2",
          updatedAt: "2022-02-09T05:53:59.884Z",
          createdAt: "2022-02-09T05:53:59.884Z",
        },
        {
          name: "SDE3",
          updatedAt: "2022-02-09T05:53:59.884Z",
          createdAt: "2022-02-09T05:53:59.884Z",
        },
        {
          name: "SDE4",
          updatedAt: "2022-02-09T05:53:59.884Z",
          createdAt: "2022-02-09T05:53:59.884Z",
        },
        {
          name: "Principal Manager",
          updatedAt: "2022-02-09T05:53:59.884Z",
          createdAt: "2022-02-09T05:53:59.884Z",
        },
        {
          name: "Manager",
          updatedAt: "2022-02-09T05:53:59.884Z",
          createdAt: "2022-02-09T05:53:59.884Z",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("profiles", null, {});
  },
};
